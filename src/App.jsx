import React, { useState } from "react";
import axios from "axios";

function App() {
  const [url, setUrl] = useState("");
  const [pageTitle, setPageTitle] = useState("");
  const [loading, setLoading] = useState(false);

  const handleSubmit = async () => {
    setLoading(true);
    try {
      const response = await axios.post("http://localhost:4001/fetch-html", {
        url,
      });
      const html = response.data.html;
      const parser = new DOMParser();
      const doc = parser.parseFromString(html, "text/html");
      const title = doc.querySelector("title");
      const pageTitle = title ? title.textContent : "No title found";
      setPageTitle(pageTitle);
      setLoading(false);
    } catch (error) {
      setPageTitle("Failed to get result");
      setLoading(false);

      console.error("Error fetching page title:", error);
    }
  };

  return (
    <div className="container">
      <div className="form">
        <input
          type="text"
          value={url}
          onChange={(e) => setUrl(e.target.value)}
          placeholder="Enter URL"
        />
        <button onClick={handleSubmit} className="btn">
          Get Page Title
        </button>
      </div>
      <>
        {loading ? (
          <div className="text">LOADING...</div>
        ) : (
          <div className="text">{pageTitle}</div>
        )}
      </>
    </div>
  );
}

export default App;
