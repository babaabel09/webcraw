const express = require("express");
const app = express();
const port = 4001;
const cors = require("cors");
const axios = require("axios");
app.use(cors());
app.use(express.json());

app.listen(port, () => {
  console.log(`server running on port ${port}`);
});

app.post("/fetch-html", async (req, res) => {
  const { url } = req.body;
  console.log(url);

  try {
    const response = await axios.get(url);
    const html = response.data;
    res.json({ html });
  } catch (error) {
    console.error("Error fetching page:", error.message);
    res.status(500).json({ error: "Failed to fetch HTML content" });
  }
});
